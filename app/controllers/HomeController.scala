package controllers

import managers._
import models._
import play.api._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import javax.inject._


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents, trkMgr : Trunk_Manager, mdsMgr: MDS_manager, mdsCallsMgr: MDS_calls_manager, flowsMgr : MDS_flows_manager) extends BaseController {
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  def apiDocs =
    Action(
      Redirect(
        s"/api/swagger-ui/index.html?url=/api/swagger.json"
      )
    )


  def get_Mds_Call_Flows() = Action { implicit request: Request[AnyContent] =>
    getDatesIfValid(request) match {
      case Left(errorResult) => errorResult
      case Right(dates) =>
        val CallFlows = flowsMgr.get_Mds_Call_Flows(mdsMgr.getMdsList(), mdsCallsMgr.getMdsCallsList(dates), trkMgr.get_Trunk_List(dates))
        Ok(Json.toJson(CallFlows))
    }
  }

  def getDatesIfValid(request: Request[AnyContent]): Either[Result, Dates] = {
    request.body.asJson.toRight(NotFound).flatMap(
      maybeDates => maybeDates.validate[Dates] match {
          case JsSuccess(dates, _) => Right(dates)
          case e: JsError => Left(BadRequest)
    })
  }
}
