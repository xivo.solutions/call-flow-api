# API project with Play Framework

This project is an API developed with the Play Framework, using the Scala programming language. It provides a set of services to query the XIVO stats database. It is linked with a Front: https://gitlab.com/xivo.solutions/call-flow-graph.git

## Facility

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/xivo.solutions/call-flow-api.git
```
Navigate to the project directory:

```bash
cd call-flow-api
```
Edit your self configuration with variable  :
```configuration
db.default {
    url="jdbc:postgresql://{DATA_BASE_IP}:port/xivo_stats"
    username=""
    password = ""
}
```


Launch the application using Sbt:
```bash
sbt run -Dconfig.file=conf/self_conf.conf
```
The API will be accessible at: http://localhost:9000

### Setup
The application configuration is in the conf/application.conf file. You can customize database access.

### Endpoints
The API provides the following endpoints:

POST /CallFlows This returns an array list of Media Server (mds), call flows (callFlow), and trunk (trunk).

### Authors
SEVELLEC Brendan
DUVAL Vincent
