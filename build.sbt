name := """play-scala-call_dat_visualisation"""
organization := "XiVo"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

scalaVersion := "2.13.11"

libraryDependencies ++= Seq(
  guice,
  jdbc,
  "org.playframework.anorm" %% "anorm-postgres" % "2.7.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
  "org.postgresql" % "postgresql" % "42.3.5",
  "org.webjars" % "swagger-ui" % "3.51.1",
  filters
)

swaggerDomainNameSpaces := Seq("models")